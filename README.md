Search Engine using Hadoop
================================

This project implements a Simple Search Engine using Apache Hadoop in Java. 

* Wikipedia English Dump Files are used as data set. 
* For indexing, Fully Inverted Indexes are used.
* For ranking of documents, Page Rank Algorithm is used. 

References 
-----------

* [Wikipedia English Dump Files] [1]
* Brin, Sergey, and Lawrence Page. "The anatomy of a large-scale hypertextual Web search engine."
* Manning, Christopher D., Prabhakar Raghavan, and Hinrich Schütze. [Introduction to information retrieval] [2].

  [1]: http://dumps.wikimedia.org/nlwiki/latest/nlwiki-latest-pages-articles.xml.bz2        "Wikipedia English Dump Files"
  [2]: http://nlp.stanford.edu/IR-book/  "Introduction to information retrieval"
