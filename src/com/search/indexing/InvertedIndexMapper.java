package com.search.indexing;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.CharacterCodingException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

/**
 * Mapper Class for creating an Inverted Index. 
 * The input format is - 
 * Key -  <key> 
 * Value - Page Text 
 * The Mapper Output is 
 * Key -  <word> 
 * Value - PageId,WordLocation
 * e.g.
 * Middle - Types_of_Schools,1 
 * High   - Types_of_Schools,20
 * Senior - Types_of_Schools,50
 *
 */
public class InvertedIndexMapper extends MapReduceBase implements
		Mapper<LongWritable, Text, Text, Text> {
	
	private HashSet<String> stopWords = new HashSet<String>();
	private final static int numCommonWords = 6000;

	public void map(LongWritable key, Text value,
			OutputCollector<Text, Text> output, Reporter reporter)
			throws IOException {

		// Returns String[0] = <title>[TITLE]</title>
		// String[1] = <text>[CONTENT]</text>
		// !! without the <tags>.
		String[] titleAndText = parseTitleAndText(value);

		String pageId = titleAndText[0];
		if (isPageValid(pageId))
			return;
		String pageText = titleAndText[1];
		StringTokenizer tokenizer = new StringTokenizer(pageText);
		if (stopWords.isEmpty()) {
			loadStopWords();
		}
		int i = 0;
		Text outkey = new Text();
		while (tokenizer.hasMoreTokens()) {
			i++;
			String word_str = scrub(tokenizer.nextToken());
			if (!stopWords.contains(word_str)) {
				outkey.set(word_str);
				output.collect(outkey, new Text(pageId + "," + i));
			}
		}
	}

	private String scrub(String noisyword) {
		return noisyword.toLowerCase().replaceAll("\\pP|\\pS", "");
	}

	private boolean isPageValid(String pageString) {
		return pageString.contains(":");
	}

	/**
	 * Read the Dictionary with Stop Words. Such words are very common words and
	 * should not be included in index because they act more as noise
	 * 
	 * @throws IOException
	 */
	private void loadStopWords() throws IOException {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		InputStream in = null;
		try {
			int countNum = 0;
			in = fs.open(new Path("/dict"));
			Scanner sc = new Scanner(in);
			while (sc.hasNext() && countNum < numCommonWords) {
				String dictword = scrub(sc.next());
				stopWords.add(dictword);
				countNum++;
			}
		} finally {
			IOUtils.closeStream(in);
		}
	}

	/**
	 * Extract Title and Body from Page Text
	 * @param value
	 * @return
	 * @throws CharacterCodingException
	 */
	private String[] parseTitleAndText(Text value)
			throws CharacterCodingException {
		String[] titleAndText = new String[2];

		int start = value.find("<title>");
		int end = value.find("</title>", start);
		start += 7; // add <title> length.

		titleAndText[0] = Text.decode(value.getBytes(), start, end - start);

		start = value.find("<text");
		start = value.find(">", start);
		end = value.find("</text>", start);
		start += 1;

		if (start == -1 || end == -1) {
			return new String[] { "", "" };
		}

		titleAndText[1] = Text.decode(value.getBytes(), start, end - start);

		return titleAndText;
	}
}