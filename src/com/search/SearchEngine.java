package com.search;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

import com.search.graphbuilder.GraphBuilderMapper;
import com.search.graphbuilder.GraphBuilderReducer;
import com.search.graphbuilder.XmlInputFormat;
import com.search.indexing.InvertedIndexMapper;
import com.search.indexing.InvertedIndexReduce;
import com.search.pagerankcalculator.PageRankCalculateMapper;
import com.search.pagerankcalculator.PageRankCalculateReduce;

/**
 * Class to initiate creation of Inverted Index 
 * and Calculation of Page Rank
 *
 */
public class SearchEngine {

	private static NumberFormat nf = new DecimalFormat("00");

	public static void main(String[] args) throws Exception {
		SearchEngine pageRanking = new SearchEngine();

		pageRanking.createInvertedIndex("wikipedia/in", "wikipedia/index");
		pageRanking.buildGraph("wikipedia/in", "wikipedia/ranking/iter00");

		for (int runs = 0; runs < 5; runs++) {
			pageRanking.runRankCalculation(
					"wikipedia/ranking/iter" + nf.format(runs), "wikipedia/ranking/iter"
							+ nf.format(runs + 1));
		}

	}

	/**
	 * Creates a Graph over pages
	 * @param inputPath
	 * @param outputPath
	 * @throws IOException
	 */
	public void buildGraph(String inputPath, String outputPath)
			throws IOException {
		JobConf conf = new JobConf(SearchEngine.class);

		conf.set(XmlInputFormat.START_TAG_KEY, "<page>");
		conf.set(XmlInputFormat.END_TAG_KEY, "</page>");

		// Input / Mapper
		FileInputFormat.setInputPaths(conf, new Path(inputPath));
		conf.setInputFormat(XmlInputFormat.class);
		conf.setMapperClass(GraphBuilderMapper.class);

		// Output / Reducer
		FileOutputFormat.setOutputPath(conf, new Path(outputPath));
		conf.setOutputFormat(TextOutputFormat.class);
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);
		conf.setReducerClass(GraphBuilderReducer.class);

		JobClient.runJob(conf);
	}

	/**
	 * Calculate Page Rank over the graph
	 * @param inputPath
	 * @param outputPath
	 * @throws IOException
	 */
	private void runRankCalculation(String inputPath, String outputPath)
			throws IOException {
		JobConf conf = new JobConf(SearchEngine.class);

		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		FileInputFormat.setInputPaths(conf, new Path(inputPath));
		FileOutputFormat.setOutputPath(conf, new Path(outputPath));

		conf.setMapperClass(PageRankCalculateMapper.class);
		conf.setReducerClass(PageRankCalculateReduce.class);

		JobClient.runJob(conf);
	}

	/**
	 * Create  Inverted Index over the corpus
	 * @param inputPath
	 * @param outputPath
	 * @throws IOException
	 */
	private void createInvertedIndex(String inputPath, String outputPath)
			throws IOException {

		JobConf conf = new JobConf(SearchEngine.class);

		conf.set(XmlInputFormat.START_TAG_KEY, "<page>");
		conf.set(XmlInputFormat.END_TAG_KEY, "</page>");

		// Input / Mapper
		FileInputFormat.setInputPaths(conf, new Path(inputPath));
		conf.setInputFormat(XmlInputFormat.class);
		conf.setMapperClass(InvertedIndexMapper.class);

		// Output / Reducer
		FileOutputFormat.setOutputPath(conf, new Path(outputPath));
		conf.setOutputFormat(TextOutputFormat.class);
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);
		conf.setReducerClass(InvertedIndexReduce.class);

		JobClient.runJob(conf);
	}

}
